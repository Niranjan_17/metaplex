import { Layout } from 'antd';
import React from 'react';
import { useConnection, useStore } from '@oyster/common';
import { useMeta } from '../../contexts';
import { SalesListView } from './components/SalesList';
import { SetupView } from './setup';
import { useWallet } from '@solana/wallet-adapter-react';
import { saveAdmin } from '../../actions/saveAdmin';


export const HomeView = () => {
  const { isLoading, store } = useMeta();
  const { isConfigured } = useStore();
  const { wallet } = useWallet();
  const connection = useConnection();
  
  const showAuctions = (store && isConfigured) || isLoading;

  return (
    <Layout style={{ margin: 0, marginTop: 30, alignItems: 'center' }}>
      {/* <button onClick={async () => {
        try {
          await saveAdmin(connection, wallet, false, [])
        } catch (e) {
          console.error(e);
        }
}}>CREATE STORE</button> */}

      {showAuctions ? <SalesListView /> : <SetupView />}
    </Layout>
  );
};
